#include "dtm.h"
#include <sstream>
#include <cassert>

DTM::DTM(std::size_t states, std::vector<std::size_t> final_states,
         std::string alphabet, std::size_t first_input, std::string content)
    : states(states), final_states(final_states), alphabet(alphabet), first_input(first_input),
      transition_table(states, std::vector<TrTuple>(this->alphabet.size())),
      content(str_to_internal_deque(content)), position(this->content.begin()), state(0)
{}

/*
void DTM::set_transition(std::size_t state, std::size_t symbol, TrTuple transition) {
    transition_table[state][symbol] = transition;
}
*/

void DTM::set_transition(std::size_t state, char read, char write, std::size_t new_state, int dir) {
    assert(state < states);
    assert(new_state < states);
    size_t iread = char_to_num(read);
    assert(iread != -1);
    size_t iwrite = char_to_num(write);
    assert(iwrite != -1);
    assert(dir >= -1 && dir <= 1);
    transition_table[state][iread] = TrTuple(new_state, iwrite, dir);
}

void DTM::reset(std::string content) {
    this->content = str_to_internal_deque(content);
    position = this->content.begin();
    state = 0;
}

std::string DTM::to_string() {
    std::ostringstream str;
    for (std::deque<std::size_t>::const_iterator iter = content.cbegin();
         iter != content.cend();
         iter++) {
        str << alphabet[*iter];
    }
    str << '\n';
    std::size_t arrow = position - content.cbegin();
    for (std::size_t i = 0; i < arrow; i++) {
        str << ' ';
    }
    str << "^\n";
    for (std::size_t i = 0; i < arrow; i++) {
        str << ' ';
    }
    str << state << '\n';
    return str.str();
}

bool DTM::step() {
    TrTuple transition = transition_table[state][*position];
    if (transition == TrTuple()) {
        return false;
    }
    else {
        state = transition.new_state;
        *position = transition.new_symbol;
        position += transition.direction;
        if (position < content.cbegin()) {
            content.push_front(0);
            position = content.begin();
        }
        else if (position >= content.cend()) {
            content.push_back(0);
            position = content.end() - 1;
        }
        return true;
    }
}

std::size_t DTM::char_to_num(char c) {
    for (std::size_t i = 0; i < alphabet.size(); i++) {
        if (c == alphabet[i]) {
            return i;
        }
    }
    return -1;
}

std::deque<std::size_t> DTM::str_to_internal_deque(std::string str) {
    std::deque<std::size_t> dq;
    if (str.size()) {
        for (char c : str) {
            std::size_t n = char_to_num(c);
            assert(n != -1);
            dq.push_back(n);
        }
    }
    else {
        dq.push_back(0);
    }
    return dq;
}

bool DTM::is_accepted() {
    for (size_t f : final_states) {
        if (state == f) {
            return true;
        }
    }
    return false;
}
