# Build
```sh
cmake .
cmake --build .
```

# Usage
```sh
./turing-machine [input]
```
