#ifndef DTM_H
#define DTM_H

#include <vector>
#include <string>
#include <deque>

class DTM
{
private:
    struct TrTuple {
        std::size_t new_state;
        std::size_t new_symbol;
        int direction;

        /*
        static const int L = -1;
        static const int R = 1;
        static const int N = 0;
        */

        /**
         * @brief Default constructed TrTuple represents an undefined transition
         */
        TrTuple()
            : new_state(-1), new_symbol(0), direction(0)
        {}

        TrTuple(std::size_t new_state, std::size_t new_symbol, int direction)
            : new_state(new_state), new_symbol(new_symbol), direction(direction)
        {}

        bool operator ==(TrTuple rhs) {
            return new_state == rhs.new_state && new_symbol == rhs.new_symbol && direction == rhs.direction;
        }
    };
    /**
     * @brief number of states, the states are 0..(states-1)
     */
    std::size_t states;

    /**
     * @brief vector containing the final states
     */
    std::vector<std::size_t> final_states;

    /**
     * @brief complete alphabet, the first symbol is blank
     */
    std::string alphabet;

    /**
     * @brief The input symbol set starts at index first_input
     */
    std::size_t first_input;

    // Transition table
    std::vector<std::vector<TrTuple>> transition_table;

    // Configuration
    std::deque<std::size_t> content;
    std::deque<std::size_t>::iterator position;
    std::size_t state;

    std::size_t char_to_num(char c);
    std::deque<std::size_t> str_to_internal_deque(std::string str);

public:
    static const int L = -1;
    static const int R = 1;
    static const int N = 0;

    /**
     * @brief Construct DTM
     * @param states
     * @param final_states
     * @param alphabet
     * @param first_input
     */
    DTM(std::size_t states, std::vector<std::size_t> final_states,
        std::string alphabet, std::size_t first_input,
        std::string content = std::string());

    // void set_transition(std::size_t state, std::size_t symbol, TrTuple transition);

    void set_transition(std::size_t state, char read, char write, std::size_t new_state, int dir);

    void reset(std::string content);

    std::string to_string();

    bool step();

    bool is_accepted();
};

#endif // DTM_H
