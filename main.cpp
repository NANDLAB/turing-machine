#include <iostream>
#include "dtm.h"
#include <vector>
#include <iomanip>

using namespace std;

int main(int argc, char *argv[])
{
    cout << "----------------INFORMATICS HOMEWORK----------------" << endl;
    const char *tm_input = "1*10";
    if (argc >= 2) {
        tm_input = argv[1];
    }
    else {
        cout << "No input string argument specified, using default " << quoted(tm_input) << "." << endl;
    }

    DTM tm2(18, {16}, "_#23*01", 4, tm_input);

    /****************  state   read  write  new_state  direction */
    tm2.set_transition(     0,   '0',   '0',         1,   DTM::R);
    tm2.set_transition(     0,   '1',   '1',         2,   DTM::R);
    tm2.set_transition(     1,   '0',   '0',         1,   DTM::R);
    tm2.set_transition(     1,   '1',   '1',         2,   DTM::R);
    tm2.set_transition(     2,   '0',   '0',         2,   DTM::R);
    tm2.set_transition(     2,   '1',   '1',         2,   DTM::R);
    tm2.set_transition(     2,   '*',   '*',         0,   DTM::R);
    tm2.set_transition(     2,   '_',   '_',         3,   DTM::R);
    tm2.set_transition(     3,   '_',   '2',         4,   DTM::L);
    tm2.set_transition(     4,   '0',   '0',         4,   DTM::L);
    tm2.set_transition(     4,   '1',   '1',         4,   DTM::L);
    tm2.set_transition(     4,   '_',   '_',         5,   DTM::L);
    tm2.set_transition(     5,   '#',   '#',         5,   DTM::L);
    tm2.set_transition(     5,   '0',   '0',         5,   DTM::L);
    tm2.set_transition(     5,   '*',   '#',        11,   DTM::R);
    tm2.set_transition(     5,   '_',   '_',        13,   DTM::R);
    tm2.set_transition(     5,   '1',   '0',         6,   DTM::R);
    tm2.set_transition(     6,   '0',   '1',         6,   DTM::R);
    tm2.set_transition(     6,   '0',   '1',         6,   DTM::R);
    tm2.set_transition(     6,   '#',   '#',         6,   DTM::R);
    tm2.set_transition(     6,   '_',   '_',         7,   DTM::R);
    tm2.set_transition(     7,   '0',   '0',         7,   DTM::R);
    tm2.set_transition(     7,   '1',   '1',         7,   DTM::R);
    tm2.set_transition(     7,   '2',   '0',         8,   DTM::R);
    tm2.set_transition(     7,   '3',   '1',         8,   DTM::R);
    tm2.set_transition(     8,   '_',   '2',         9,   DTM::L);
    tm2.set_transition(     8,   '0',   '2',         9,   DTM::L);
    tm2.set_transition(     8,   '1',   '3',         9,   DTM::L);
    tm2.set_transition(     9,   '0',   '0',         9,   DTM::L);
    tm2.set_transition(     9,   '1',   '1',         9,   DTM::L);
    tm2.set_transition(     9,   '_',   '_',         5,   DTM::L);
    tm2.set_transition(    10,   '0',   '2',         4,   DTM::L);
    tm2.set_transition(    10,   '1',   '3',         4,   DTM::L);
    tm2.set_transition(    11,   '0',   '#',        11,   DTM::R);
    tm2.set_transition(    11,   '#',   '#',        11,   DTM::R);
    tm2.set_transition(    11,   '_',   '_',        12,   DTM::R);

    tm2.set_transition(    12,   '0',   '0',        12,   DTM::R);
    tm2.set_transition(    12,   '1',   '1',        12,   DTM::R);
    tm2.set_transition(    12,   '2',   '1',        17,   DTM::L);
    // Halts, if it sees a 3.

    tm2.set_transition(    17,   '0',   '0',        17,   DTM::L);
    tm2.set_transition(    17,   '1',   '1',        17,   DTM::L);
    tm2.set_transition(    17,   '_',   '_',        10,   DTM::R);

    tm2.set_transition(    13,   '0',   '#',        13,   DTM::R);
    tm2.set_transition(    13,   '#',   '#',        13,   DTM::R);
    tm2.set_transition(    13,   '_',   '_',        14,   DTM::R);
    tm2.set_transition(    14,   '0',   '0',        15,   DTM::R);
    tm2.set_transition(    15,   '1',   '1',        15,   DTM::R);
    tm2.set_transition(    15,   '2',   '1',        15,   DTM::R);
    // Halts, if it sees a 3.

    tm2.set_transition(    15,   '_',   '_',        16,   DTM::N);

    do {
        cout << tm2.to_string() << endl;
    }
    while (tm2.step());
    cout << "Accepted: " << (tm2.is_accepted() ? "Yes" : "No") << endl;
    return 0;
}
